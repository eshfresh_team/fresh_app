import uuid
import json
from app_admin.mixins import CustomerContextMixin, WithHeader, JSONResponseMixin
from app_customer.forms import RegistrationForm
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, render_to_response
from core import models
from dj_userland.mails import mail_body
from django.utils import timezone
from django.views.generic import ListView, View
from django.contrib.auth import authenticate, login, logout
from dj_userland import settings
from core import const

class Index(ListView, CustomerContextMixin, WithHeader):
    current_page = 'index'
    page_header = 'Главная'
    context_object_name = 'products'
    template_name = 'product-tiles.html'
    queryset = models.Product.objects.all() #TODO: there should be NO queryset, as model loads as json via AJAX
    ordering = 'id'


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        # form.clean()
        if form.is_valid():
            user = models.User.objects.create_user(
                password=form.cleaned_data['password1'],
                email=form.cleaned_data['email'],
            )
            user.first_name=form.cleaned_data['first_name']
            user.last_name=form.cleaned_data['last_name']
            user.save()

            return redirect(to='/')
    else:
        form = RegistrationForm()

    return render(request, 'register.html', context={'form': form})

def customer_login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(username=email, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(to=reverse('site_index'))
            else:
                err_text = 'Пользователь не активен'
        else:
            err_text = 'Неверный логин или пароль'

        return render(request, 'c_login.html', context={
            "error": err_text,
        })

    else:
        ctx = {'already_logged': False}
        if request.user.is_authenticated():
            ctx['already_logged'] = True

        return render(request, 'c_login.html', context=ctx)

def customer_logout(request):
    logout(request)
    return redirect(to=reverse('site_index'))

'''
    Customer
    cart views
'''

# def cart_view(request):
#     return render(request, 'cart.html')


class Cart(ListView, CustomerContextMixin, WithHeader):
    # TODO: !!! There should be section with suggested goods at the top of an empty cart !!!
    template_name = 'cart.html'
    current_page = 'index'
    page_header = 'Главная'
    context_object_name = 'products'
    queryset = models.Product.objects.all() #TODO: there should be NO queryset, as model loads as json via AJAX
    ordering = 'id'


class SuggestedProducts(ListView, CustomerContextMixin, WithHeader):
    template_name = 'cart.html'
    current_page = 'index'
    context_object_name = 'products'
    queryset = models.Product.objects.all() #TODO: there should be NO queryset, as model loads as json via AJAX
    ordering = 'id'


class AddToCart():
    pass


def add_to_cart(request, product_id, product_count):
    prod_data = []
    cart = request.session.get('cart', None)
    product_id = int(product_id)
    product_count = int(product_count)
    status = 0
    found = 0
    product = models.Product.objects.get(pk=product_id)
    minAmount = product.minimal_amount
    if cart is not None:
        d_cart = json.loads(cart)
        prod_data = d_cart # this line is here for sure that the deal is with list
    else:
        request.session['cart'] = ""
    for p in prod_data:
        if p['id'] == product_id:
            p['count'] = product_count
            found = 1
    if not found:
        prod_data.append({'id': product_id, 'count': product_count})

    s_cart = json.dumps(prod_data)
    request.session['cart'] = s_cart
    status = json.dumps({'status': status, 'cart': prod_data})

    return HttpResponse(status, content_type='application/json')


# TODO: reorganize this method as 'update_context_info' method!!!
def update_cart(request):
    cart = request.session.get('cart', None)
    status = 0
    d_cart = []
    if cart is not None:
        d_cart = json.loads(cart)

    data = {'status': status, 'cart': d_cart}

    # update customer info
    if request.user.is_authenticated():
        data.update({'customer_email': request.user.email})

    # update context info
    data.update({'settings': settings.SITE_SETTINGS})

    return HttpResponse(json.dumps(data), content_type='application/json')

def delete_from_cart(request, product_id):
    prod_data = []
    cart = request.session.get('cart', None)
    product_id = int(product_id)
    status = 0
    if cart is not None:
        d_cart = json.loads(cart)
        prod_data = d_cart # TODO: is this line necessary? then we exactly know that type of prod_ids is list, not other!
        try:
            deleted = None
            for i,p in enumerate(prod_data):
                if p['id'] == product_id:
                    deleted = prod_data.pop(i)
            if deleted is None:
                status = 3
        except KeyError:
            status = 1
            raise Http404("Sorry, but index is out of range.")
        except:
            status = 2
            raise Http404("Unhandled exception.")
        if len(prod_data) == 0:
            del request.session['cart']
        else:
            request.session['cart'] = json.dumps(prod_data)
    else:
        request.session['cart'] = ""
    status = json.dumps({'status': status})

    return HttpResponse(status, content_type='application/json')

def remove_all(request):
    cart = request.session.get('cart', None)
    status = 0
    if cart is not None:
        del request.session['cart']
    else:
        status = 1
    status = json.dumps({'status': status})

    return HttpResponse(status, content_type='application/json')


class Orders(ListView, CustomerContextMixin, WithHeader):
    template_name = 'orders.html'
    current_page = 'orders'
    page_header = 'История заказов'
    context_object_name = 'orders'

    def get_queryset(self):
        return None


class OrderHistory(ListView, JSONResponseMixin, CustomerContextMixin, WithHeader):
    current_page = 'orders'
    page_header = 'История заказов'

    def get_context_data(self, **kwargs):
        ctx = super(OrderHistory, self).get_context_data()
        ctx['orders_length'] = len(self.object_list)

        return ctx

    def get_queryset(self):
        orders = models.Order.objects.filter(customer=self.request.user)

        return orders

    def render_to_response(self, context, **response_kwargs):
        resp = []
        for obj in self.object_list:
            resp.append(obj.to_json())

        return HttpResponse(json.dumps(resp), content_type='application/json')

def create_order(request):
    ip = request.environ.get('REMOTE_ADDR', None)
    c = None
    if request.is_ajax() and request.method == 'POST':
        cart = request.session.get('cart', None)
        email = request.POST.get('email', None)
        phone = request.POST.get('phone', None)
        total = request.POST.get('total', None)
        positions = json.loads(request.POST.get('positions', None))

        if request.user.is_authenticated():
            c = request.user

        if cart is not None and email and phone and total and positions:
            try:
                order_number = str(uuid.uuid1())[:8]
                order = models.Order(customer=c, number=order_number, user_phone=phone, user_email=email, total=total)
                # order_items_list = json.loads(cart)
                order.save()

                for val in positions:
                    prod = models.Product.objects.get(pk=val['id'])
                    item = models.OrderItem(product=prod, order=order, count=int(val['count']),
                                            amount=float(val['amount']), _price=val['price'])
                    item.save()
                    order.orderitem_set.add(item)
                    decrease = prod.left_in_stock - int(val['count'])
                    prod.left_in_stock = decrease
                    prod.save()
                order.save()
                del request.session['cart']
            # TODO: catch all possible exceptions here!!
            except IntegrityError:
                raise Http404("Sorry, value of key is duplicated!")
            except:
                raise Http404("Unhandled exception.")

            ctx = {
                'status': 0,
                'order': order.to_json(),
                'error': None,
                'success': 'Ваш заказ принят, спасибо!'
            }
            body = mail_body(ctx)
            status = send_mail('Ваш заказ в магазине фермерских продуктов ешьфреш.рф', body, settings.ADMIN_EMAIL,
                    [email], fail_silently=settings.MAIL_FAIL_SILENT, auth_user=settings.EMAIL_HOST_USER,
                    auth_password=settings.EMAIL_HOST_PASSWORD, html_message=body)
            body += ';\r\n IP is [%s]' % ip
            status2 = send_mail('Новый заказ!', body, settings.ADMIN_EMAIL,
                    [settings.ADMIN_EMAIL], fail_silently=settings.MAIL_FAIL_SILENT, auth_user=settings.EMAIL_HOST_USER,
                    auth_password=settings.EMAIL_HOST_PASSWORD, html_message=body)

            ctx.update({'customer_stat': status, 'admin_stat': status2})
        else:
            ctx = {
                'status': 1,
                'error': 'cart OR email OR phone OR total Or positions is None',
            }
    else:
        ctx = {
            'status': 2,
            'error': 'Method not allowed',
        }

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def create_feedback(request):
    voted = request.session.get('voted', None)
    ip = request.environ.get('REMOTE_ADDR', None)
    if voted is not None:
        if (int(timezone.now().timestamp()) - int(voted)) > const.TIME_TO_VOTE:
            del request.session['voted']
    if request.is_ajax() and request.method == 'POST' and not voted:
        email = request.POST.get('email', None)
        feedback = request.POST.get('feedback', None)
        if email and feedback:
            try:
                body = "%s wrote feedback on \'elec-all.ru\': ``%s``; IP is [%s]" % (email, ip, feedback)
                status = send_mail('Feedback from elec-all.ru', body, settings.ADMIN_EMAIL,
                    [settings.ADMIN_EMAIL], fail_silently=settings.MAIL_FAIL_SILENT, auth_user=settings.EMAIL_HOST_USER,
                    auth_password=settings.EMAIL_HOST_PASSWORD)
                ctx = {'mail_stat': status}
            except:
                raise Http404("Unhandled exception.")

            ctx.update({
                'status': 0,
                'error': None,
                'success': 'Вы успешно отправили отзыв, спасибо!'
            })

            request.session['voted'] = int(timezone.now().timestamp())
        else:
            ctx = {
                'status': 1,
                'error': 'email OR feedback is None',
                'success': None
            }
    elif voted:
        ctx = {
            'status': 3,
            'success': 'Спасибо, вы уже написали отзыв!',
            'error': 'voted already',
        }
    else:
        ctx = {
            'status': 2,
            'error': 'Method not allowed',
            'success': None
        }

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def create_purchase(request):
    if request.is_ajax() and request.method == 'POST':
        email = request.POST.get('email', None)
        purchase = request.POST.get('purchase', None)
        ip = request.environ.get('REMOTE_ADDR', None)
        if email and purchase:
            try:
                body = "%s wrote purchase on \'elec-all.ru\'. IP is: [%s] He wants to know about: ``%s``" % (email, ip, purchase)
                status = send_mail('Purchase on elec-all.ru', body, settings.ADMIN_EMAIL,
                    [settings.ADMIN_EMAIL], fail_silently=settings.MAIL_FAIL_SILENT, auth_user=settings.EMAIL_HOST_USER,
                    auth_password=settings.EMAIL_HOST_PASSWORD)
                ctx = {'mail_stat': status}
            except:
                raise Http404("Unhandled exception.")

            ctx.update({
                'status': 0,
                'error': None,
                'success': 'Спасибо за интерес к нашей компании! Мы свяжемся с вами, как только получим информацию о интересующей вас позиции!'
            })
        else:
            ctx = {
                'status': 1,
                'error': 'email OR feedback is None',
                'success': None
            }
    else:
        ctx = {
            'status': 2,
            'error': 'Method not allowed',
            'success': None
        }

    return HttpResponse(json.dumps(ctx), content_type='application/json')

