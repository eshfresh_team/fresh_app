from django.conf.urls import url
from app_customer import views

patterns = [
    # main
    # url(r'^$', views.AdminIndex.as_view(), name='admin_index'),
    # url(r'^login/$', views.admin_login, name='login'),
    # url(r'^logout/$', views.admin_logout, name='logout'),
    url(r'^$', views.Index.as_view(), name='site_index'),
    url(r'^login/$', views.customer_login, name='c_login'),
    url(r'^logout/$', views.customer_logout, name='c_logout'),

    # shopping cart
    url(r'^cart/$', views.Cart.as_view(), name='cart'),
    url(r'^cart/update/$', views.update_cart, name='update_cart'),
    url(r'^cart/remove_all/$', views.remove_all, name='remove_all_from_cart'),
    url(r'^cart/add/(?P<product_id>[0-9]+)&(?P<product_count>[0-9]+)/$', views.add_to_cart, name='add_to_cart'),
    url(r'^cart/delete/(?P<product_id>[0-9]+)/$', views.delete_from_cart, name='delete_from_cart'),

    # orders
    url(r'^orders/$', views.Orders.as_view(), name='orders'),
    url(r'^orders/history', views.OrderHistory.as_view(), name='o_history'),
    url(r'^orders/create/$', views.create_order, name='create_order'),
    url(r'^feedback/$', views.Index.as_view(), name='create_feedback'),
    url(r'^purchase/$', views.Index.as_view(), name='create_purchase'),

    # register
    url(r'^register/$', views.register, name='register'),

]