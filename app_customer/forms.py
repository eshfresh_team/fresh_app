from django import forms
from core import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


class RegistrationForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Email адрес"),
                             error_messages={ 'invalid': _('Email уже существует') })
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30,
        render_value=False)), label=_("Пароль"), error_messages={ 'invalid': _('Пароли не совпадают') })
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30,
                                                                      render_value=False)), label=_("Пароль (еще раз)"))
    first_name = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)),
                                label=_('Имя'),
                                error_messages={ 'invalid': _('Имя может содержать только буквы') })
    last_name = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)),
                                label=_('Фамилия'),
                                error_messages={ 'invalid': _('Фамилия может содержать только буквы') })


    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields did not match."))

        try:
            models.User = get_user_model()
            user = models.User.objects.get(email__iexact=self.cleaned_data['email'])
        except models.User.DoesNotExist:
            pass
        else:
            raise forms.ValidationError(_("The username already exists. Please try another one."))

        return self.cleaned_data