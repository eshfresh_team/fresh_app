/**
 * Created by sanya on 17.03.16.
 */
;

function Product(id, name, desc, cost, inStock, leftInStock, images, min, units, seller, arrival, isInCart, count) {
	var self = this;
	this.id = id;
    this.name = name;
    this.desc = desc;
    this.cost = cost;
    this.inStock = inStock; // boolean field!!
    this.leftInStock = leftInStock;
    this.images = images;
    this.units = units;
    this.minAmount = min;
    this.seller = seller;
    this.arrivalAt = arrival;

    this.arrivalAtString = ko.computed(function() {
        var year = self.arrivalAt.split("T")[0].split("-")[0];
        var month = self.arrivalAt.split("T")[0].split("-")[1];
        var day = self.arrivalAt.split("T")[0].split("-")[2];
        var date = new Date(year, month-1, day);
        var dayOfWeek = date.toDateString().split(" ")[0];
        var monthStr;
        var dayOfWeekStr;
        switch (parseInt(month)) {
            case 1: 
                monthStr = "января";
                break;
            case 2: 
                monthStr = "февраля";
                break;
            case 3: 
                monthStr = "марта";
                break;
            case 4: 
                monthStr = "апреля";
                break;
            case 5: 
                monthStr = "мая";
                break;
            case 6: 
                monthStr = "июня";
                break;
            case 7: 
                monthStr = "июля";
                break;
            case 8: 
                monthStr = "августа";
                break;
            case 9: 
                monthStr = "сентября";
                break;
            case 10: 
                monthStr = "октября";
                break;
            case 11: 
                monthStr = "ноября";
                break;
            case 12: 
                monthStr = "декабря";
                break;
        }
        switch (dayOfWeek) {
            case "Sun": 
                dayOfWeekStr = "вс";
                break;
            case "Mon": 
                dayOfWeekStr = "пн";
                break;
            case "Tue": 
                dayOfWeekStr = "вт";
                break;
            case "Wed": 
                dayOfWeekStr = "ср";
                break;
            case "Thu": 
                dayOfWeekStr = "чт";
                break;
            case "Fri": 
                dayOfWeekStr = "пт";
                break;
            case "Sat": 
                dayOfWeekStr = "сб";
                break;
        }

        return dayOfWeekStr + ", " + day + " " + monthStr;

    });

    if(isInCart) {
    	this.isInCart = ko.observable(true);
    } else {
    	this.isInCart = ko.observable(false);
    }
    if (count) {
        this.count = ko.observable(count);
    } else {
        this.count = ko.observable(1);
    }
    this.positionAmount = ko.computed(function() {
        return (self.count() * self.minAmount).toFixed(1);
    });
    this.pureCount = count;
    this.count.subscribe(function(value) {
	    this.pureCount = value;
	});
	this.price = ko.computed(function() {
    	if(self.count() > 1) {
    		return self.cost * self.count();
    	}
    	
    	return self.cost;
    });
    this.increaseCount = function() {
        self.count(self.count() + 1);
    };
    this.decreaseCount = function() {
        self.count(self.count() - 1);
    };
}

function Order(date, number, phone, email, products, total, msg) {
    this.date = date;
    this.number = number;
    this.phone = phone;
    this.email = email;
    this.items = products;
    this.total = total;
    this.success = msg;
}

function Waiter() {
    var self = this;
    self.status = ko.observable(false);
    self.show = function () {
        self.status(true);
    };
    self.hide = function () {
        self.status(false);
    };
}

function Settings(adminEmail, shopEmail, siteName, cSiteName, minOrderSum) {
    this.adminEmail = adminEmail;
    this.shopEmail = shopEmail;
    this.siteName = siteName;
    this.cSiteName = cSiteName;
    this.minOrderSum = minOrderSum;
}