from django.conf.urls import url
from . import views

patterns = [
    # main
    url(r'^$', views.AdminIndex.as_view(), name='admin_index'),
    url(r'^login/$', views.admin_login, name='login'),
    url(r'^logout/$', views.admin_logout, name='logout'),

    # clients
    url('clients/$', views.ClientList.as_view(), name='clients_list'),
    url('clients/add/$', views.ClientCreate.as_view(), name='client_add'),
    url('clients/(?P<pk>[0-9]+)/$', views.ClientUpdate.as_view(), name='client_update'),
    url('clients/(?P<pk>[0-9]+)/disable/$', views.ClientToggleActive.as_view(), name='client_toggle_active'),

    # sellers
    url('sellers/$', views.SellerList.as_view(), name='sellers_list'),
    url('sellers/add/$', views.SellerCreate.as_view(), name='seller_add'),
    url('sellers/(?P<pk>[0-9]+)/$', views.SellerUpdate.as_view(), name='seller_update'),
    url('sellers/(?P<pk>[0-9]+)/disable/$', views.SellerToggleActive.as_view(), name='seller_toggle_active'),
    url('sellers/category/(?P<pk>\d+)/$', views.SellerList.as_view(), name='sellers_by_category'),

    # cities
    url('cities/$', views.CityList.as_view(), name='cities_list'),
    url('cities/add/$', views.CityCreate.as_view(), name='city_add'),
    url('cities/(?P<pk>[0-9]+)/$', views.CityUpdate.as_view(), name='city_update'),

    # categories
    url('categories/$', views.CategoryList.as_view(), name='categories_list'),
    url('categories/add/$', views.CategoryCreate.as_view(), name='category_add'),
    url('categories/(?P<pk>[0-9]+)/$', views.CategoryUpdate.as_view(), name='category_update'),

    # products
    url('products/$', views.ProductList.as_view(), name='products_list'),
    url('products/add/$', views.ProductCreate.as_view(), name='product_add'),
    url('products/(?P<pk>[0-9]+)/$', views.ProductUpdate.as_view(), name='product_update'),
    url('products/(?P<pk>[0-9]+)/disable/$', views.ProductToggleActive.as_view(), name='product_toggle_active'),
    url('products/remove/(?P<pk>[0-9]+)/$', views.ProductRemove.as_view(), name='product_remove'),

    # images
    url(r'^images/$', views.ImageList.as_view(), name='images_list'),
    url(r'^images/add/$', views.ImageCreate.as_view(), name='image_add'),
    # url(r'^images/remove/(?P<pk>[0-9]+)/$', views.ImageRemove.as_view(), name='image_remove'),

    # product images

    url(r'^products/(?P<pk>[0-9]+)/images/$', views.ImageListTmp.as_view(), name='product_images_list'),
    url(r'^products/(?P<pk>[0-9]+)/images/add/$', views.ImageCreateTmp.as_view(), name='add_image_to_product'),
    url(r'^products/(?P<p_id>[0-9]+)/images/(?P<pk>[0-9]+)/remove/$', views.ImageRemove.as_view(), name='image_remove'),

    # units
    url('units/$', views.UnitList.as_view(), name='units_list'),
    url('units/add/$', views.UnitCreate.as_view(), name='unit_add'),
    url('units/(?P<pk>[0-9]+)/$', views.UnitUpdate.as_view(), name='unit_update'),

    # orders
    url('orders/$', views.OrderList.as_view(), name='orders_list'),
    url('orders/add/$', views.OrderCreate.as_view(), name='order_add'),
    url('orders/(?P<pk>[0-9]+)/$', views.OrderUpdate.as_view(), name='order_update'),
]