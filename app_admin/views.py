import os
import uuid

from PIL import Image
from django.http import Http404
from django.views.generic import View, ListView, CreateView, UpdateView, TemplateView, DeleteView
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse

from .mixins import AdminContextMixin, WithHeader, WithParent
from core import models
from core import const as constants



def admin_login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(username=email, password=password)

        if user is not None:
            if user.is_active and user.type_of is models.USER_TYPE_ADMIN :
                login(request, user)
                return redirect(to='/admin')
            else:
                err_text = 'Пользователь не активен'
        else:
            err_text = 'Неверный логин или пароль'

        return render(request, 'login.html', context={
            "error": err_text,
        })

    else:
        ctx = {'already_logged': False}
        if request.user.is_authenticated():
            ctx['already_logged'] = True

        return render(request, 'login.html', context=ctx)


def admin_logout(request):
    logout(request)
    return redirect(to=reverse('login'))


class AdminIndex(TemplateView, AdminContextMixin, WithHeader):
    page_header = "Панель управления"
    template_name = 'index.html'
    current_page = 'main'


# TODO: make page_header field as 'Добавить' for all creation views,
# TODO: to avoid duplicated info, and set page_header value in superclass

# clients views
class ClientList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Клиенты'
    context_object_name = 'clients'
    template_name = 'clients/list.html'
    queryset = models.User.objects.filter(type_of=models.USER_TYPE_CUSTOMER)
    ordering = 'id'
    current_group = 'users'
    current_page = 'clients'


class ClientCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    template_name = 'clients/create.html'
    page_header = 'Добавить клиента'
    parent_title = 'Клиенты'
    current_group = 'users'
    current_page = 'clients'

    model = models.User
    fields = ['email', 'first_name', 'last_name', 'phone', 'city', 'address',]
    context_object_name = 'client'

    def get_parent_link(self):
        return reverse('clients_list')

    def form_valid(self, form):
        client = form.save(commit=False)
        client.type_of = models.USER_TYPE_CUSTOMER
        client.save()
        return super(ClientCreate, self).form_valid(form)

    def get_success_url(self):
        return '../%s/' % self.object.id


class ClientUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    template_name = 'clients/update.html'
    page_header = 'Редактировать клиента'
    parent_title = 'Клиенты'
    current_group = 'users'
    current_page = 'clients'

    model = models.User
    fields = ['email', 'first_name', 'last_name', 'phone', 'city', 'address', ]
    context_object_name = 'client'

    def get_parent_link(self):
        return reverse('clients_list')

    def get_object(self, queryset=None):
        return get_object_or_404(
                self.model,
                type_of=models.USER_TYPE_CUSTOMER,
                id=self.kwargs['pk']
        )

    def get_success_url(self):
        return '../%s/' % self.object.id


class ClientToggleActive(AdminContextMixin):
    model = models.User

    def post(self, request, *args, **kwargs):
        client = get_object_or_404(
                self.model,
                type_of=models.USER_TYPE_CUSTOMER,
                id=self.kwargs['pk']
        )

        client.is_active = not client.is_active
        client.save()

        return redirect('../')


# sellers views
class SellerList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Продавцы'
    context_object_name = 'sellers'
    template_name = 'sellers/list.html'
    ordering = 'id'
    current_group = 'users'
    current_page = 'sellers'

    def get_filter_category(self):
        cat_id = self.request.GET.get('cat_id', '')
        if cat_id == '':
            return None
        else:
            return int(cat_id)

    def get_context_data(self, *args, **kwargs):
        ctx = super(SellerList, self).get_context_data(*args, **kwargs)
        ctx['categories'] = models.Category.objects.all()
        ctx['filter_by_cat'] = self.get_filter_category()
        return ctx

    def get_queryset(self, *args, **kwargs):
        cat_id = self.get_filter_category()

        if cat_id is not None:
            self.category = get_object_or_404(
                models.Category,
                id=cat_id
            )
            return self.category.get_sellers_in_category()
        else:
            return models.User.objects.filter(type_of=models.USER_TYPE_SELLER)


class SellerCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить продавца'
    parent_title = 'Продавцы'
    current_group = 'users'
    current_page = 'sellers'

    model = models.User
    fields = ['email', 'first_name', 'last_name', 'phone', 'city', 'address',]
    context_object_name = 'seller'
    template_name = 'sellers/create.html'

    def get_parent_link(self):
        return reverse('sellers_list')

    def form_valid(self, form):
        seller = form.save(commit=False)
        seller.type_of = models.USER_TYPE_SELLER
        seller.save()
        return super(SellerCreate, self).form_valid(form)

    def get_success_url(self):
        return '../%s/' % self.object.id


class SellerUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Редактировать продавца'
    parent_title = 'Продавцы'
    current_group = 'users'
    current_page = 'sellers'

    model = models.User
    fields = ['email', 'first_name', 'last_name', 'phone', 'city', 'address', ]
    context_object_name = 'seller'
    template_name = 'sellers/update.html'

    def get_parent_link(self):
        return reverse('sellers_list')

    def get_object(self, queryset=None):
        return get_object_or_404(
                self.model,
                type_of=models.USER_TYPE_SELLER,
                id=self.kwargs['pk']
        )

    def get_success_url(self):
        return '../%s/' % self.object.id


class SellerToggleActive(AdminContextMixin):
    model = models.User

    def post(self, request, *args, **kwargs):
        seller = get_object_or_404(
                self.model,
                type_of=models.USER_TYPE_SELLER,
                id=self.kwargs['pk']
        )

        seller.is_active = not seller.is_active
        seller.save()

        return redirect('../')


class CityList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Города'
    context_object_name = 'cities'
    template_name = 'cities/list.html'
    queryset = models.City.objects.all()
    ordering = 'id'
    current_page = 'cities'
    current_group = 'dicts'


class CityCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить город'
    parent_title = 'Города'
    current_page = 'cities'
    current_group = 'dicts'

    model = models.City
    fields = ['name', 'timezone', ]
    context_object_name = 'city'
    template_name = 'cities/create.html'

    def get_parent_link(self):
        return reverse('cities_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class CityUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Редактировать город'
    parent_title = 'Города'
    current_page = 'cities'
    current_group = 'dicts'

    model = models.City
    fields = ['name', 'timezone', ]
    context_object_name = 'city'
    template_name = 'cities/update.html'

    def get_parent_link(self):
        return reverse('cities_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class CategoryList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Категории'
    context_object_name = 'categories'
    template_name = 'categories/list.html'
    queryset = models.Category.objects.all()
    ordering = 'id'
    current_page = 'categories'
    current_group = 'dicts'



class CategoryCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить категорию'
    parent_title = 'Категории'
    current_page = 'categories'
    current_group = 'dicts'

    model = models.Category
    fields = ['name', ]
    context_object_name = 'category'
    template_name = 'categories/create.html'

    def get_parent_link(self):
        return reverse('categories_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class CategoryUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Редактировать категорию'
    parent_title = 'Категории'
    current_page = 'categories'
    current_group = 'dicts'

    model = models.Category
    fields = ['name', ]
    context_object_name = 'category'
    template_name = 'categories/update.html'

    def get_parent_link(self):
        return reverse('categories_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class ProductList(ListView, AdminContextMixin, WithHeader):
    queryset = models.Product.objects.all()
    template_name = 'products/list.html'
    ordering = 'id'

    page_header = 'Продукты'
    context_object_name = 'products'
    current_page = 'products'


class ProductCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить продукт'
    parent_title = 'Продукты'
    current_page = 'products'

    model = models.Product

    # TODO later: add image field to product creation view
    fields = ['seller', 'name', 'category', 'description',
              'minimal_amount', 'units', 'price_per_min_amount',
              'left_in_stock', 'max_saleable_amount', 'delivery_time']
    context_object_name = 'product'
    template_name = 'products/create.html'

    def get_form(self, *args, **kwargs):
        form = super(ProductCreate, self).get_form(*args, **kwargs)
        form.fields['seller'].queryset = models.User.objects.filter(type_of=models.USER_TYPE_SELLER)
        return form
    # TODO: move this implementation into models
    def form_valid(self, form):
        product = form.save(commit=False)
        category = product.category
        seller = product.seller
        sc = models.SellerCategory(seller=seller, category=category)
        sc.save()
        product.save()

        return super(ProductCreate, self).form_valid(form)

    def get_parent_link(self):
        return reverse('products_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class ProductUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Редактировать продукт'
    parent_title = 'Продукты'
    current_page = 'products'

    model = models.Product
    fields = ['seller', 'name', 'category', 'description',
              'minimal_amount', 'units', 'price_per_min_amount',
              'left_in_stock', 'max_saleable_amount', 'delivery_time']
    context_object_name = 'product'
    template_name = 'products/update.html'

    def get_form(self, *args, **kwargs):
        form = super(ProductUpdate, self).get_form(*args, **kwargs)
        form.fields['seller'].queryset = models.User.objects.filter(type_of=models.USER_TYPE_SELLER)
        # form.fields['images'].queryset = self.model.images

        return form
    # TODO: why i can't find self.object?
    def get_queryset(self):
        qs = super(ProductUpdate, self).get_queryset()

        return qs

    def get_context_data(self, **kwargs):
        ctx = super(ProductUpdate, self).get_context_data(**kwargs)
        product = models.Product.objects.get(pk=self.kwargs.get('pk', None))
        ctx['images'] = product.images

        return ctx

    def dispatch(self, request, *args, **kwargs):
        o = super(ProductUpdate, self).dispatch(request, *args, **kwargs)

        return o

    def get_parent_link(self):
        return reverse('products_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class ProductToggleActive(AdminContextMixin):
    model = models.Product

    def post(self, request, *args, **kwargs):
        product = get_object_or_404(
                self.model,
                id=self.kwargs['pk']
        )

        product.enabled = not product.enabled
        product.save()

        return redirect('../')


class ProductRemove(DeleteView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Удалить продукт'
    parent_title = 'Продукты'
    current_page = 'products'

    model = models.Product

    def get_context_data(self, **kwargs):
        ctx = super(ProductRemove, self).get_context_data(**kwargs)

        return ctx

    # TODO later: add image field to product creation view
    fields = ['seller', 'name', 'category', 'description',
              'minimal_amount', 'units', 'price_per_min_amount', 'left_in_stock', 'max_saleable_amount']
    template_name = 'products/remove.html'

    def get_parent_link(self):
        return reverse('products_list')

    def get_success_url(self):
        return reverse('products_list')

class UnitList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Единицы измерения'
    context_object_name = 'units'
    template_name = 'units/list.html'
    queryset = models.Unit.objects.all()
    ordering = 'id'
    current_page = 'units'
    current_group = 'dicts'


class UnitCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить единицу измерения'
    parent_title = 'Единицы измерения'
    current_page = 'units'
    current_group = 'dicts'

    model = models.Unit
    fields = ['name', 'short_name' ]
    context_object_name = 'unit'
    template_name = 'units/create.html'

    def get_parent_link(self):
        return reverse('units_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class UnitUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Редактировать единицу измерения'
    parent_title = 'Единицы измерения'
    current_page = 'units'
    current_group = 'dicts'

    model = models.Unit
    fields = ['name', 'short_name' ]
    context_object_name = 'unit'
    template_name = 'units/update.html'

    def get_parent_link(self):
        return reverse('units_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class OrderList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Заказы'
    context_object_name = 'orders'
    template_name = 'orders/list.html'
    queryset = models.Order.objects.all()
    current_page = 'orders'


class OrderCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить заказ'
    parent_title = 'Заказы'
    current_page = 'orders'

    model = models.Order
    fields = ['customer', 'date', 'summ', 'discount', 'comment', 'status' ]
    context_object_name = 'order'
    template_name = 'orders/create.html'

    def get_form(self, form_class=None):
        form = super(OrderCreate, self).get_form()
        products = models.Product.objects.all()

        return form

    def get_parent_link(self):
        return reverse('orders_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class OrderUpdate(UpdateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Обновить статус заказа'
    parent_title = 'Заказы'
    current_page = 'units'

    model = models.Order
    fields = ['status' ]
    context_object_name = 'order'
    template_name = 'orders/update.html'

    def get_parent_link(self):
        return reverse('orders_list')

    def get_success_url(self):
        return '../%s/' % self.object.id


class ImageList(ListView, AdminContextMixin, WithHeader):
    page_header = 'Изображения'
    current_page = 'images'
    template_name = 'images/list.html'
    queryset = models.ProductImages.objects.all()
    context_object_name = 'images'
    ordering = 'id'

    def get_context_data(self, **kwargs):
        ctx = super(ImageList, self).get_context_data()
        ctx['images'] = self.get_queryset()

        return ctx


class ImageCreate(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить изображение'
    parent_title = 'Изображения'
    template_name = 'images/create.html'
    current_page = 'images'

    model = models.ProductImages
    fields = ['product', 'image' ]
    context_object_name = 'image'

    def get_context_data(self, **kwargs):
        ctx = super(ImageCreate, self).get_context_data()
        ctx['form'] = self.get_form()

        return ctx

    def post(self, request, *args, **kwargs):
        image = self.request.FILES.get('image')
        name = image.name
        splitted = name.split('.')
        ext_idx = len(splitted) - 1
        ext = splitted[ext_idx]
        image.name = str(uuid.uuid4()) + '.' + str(ext)

        product_id = self.request.POST.get('product')

        return super(ImageCreate, self).post(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super(ImageCreate, self).get_form()

        return form

    def form_invalid(self, form):

        return super(ImageCreate, self).form_invalid(form)

    def form_valid(self, form):
        image = form.save(commit=False)
        product_id = self.request.POST.get('product')
        product = models.Product.objects.get(id=product_id)
        image.product = product
        image.save()

        size = (constants.THUMB_SIZE_HEIGHT, constants.THUMB_SIZE_WIDTH)

        full_link = image.image.file.name
        infile = Image.open(full_link)
        outfile = infile.fp.name.split('.')[0] + '_thumb' + '.jpg'

        if infile != outfile:
            try:
                infile.thumbnail(size)
                infile.save(outfile, "JPEG")
            except IOError:
                print("cannot create thumbnail for", infile)

        rel_name_spl = outfile.split('/')
        file_idx = len(rel_name_spl) - 1
        dir_idx = len(rel_name_spl) - 2
        image.thumb = rel_name_spl[dir_idx] + '/' + rel_name_spl[file_idx]
        image.save()


        return super(ImageCreate, self).form_valid(form)

    def get_parent_link(self):
        return reverse('images_list')

    def get_success_url(self):
        return reverse('image_add')


class ImageRemove(DeleteView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Удалить изображение'
    current_page = 'images'
    template_name = 'images/remove.html'
    img_link = ''
    thumb_link = ''

    model = models.ProductImages

    def get_object(self, queryset=None):
        o = super(ImageRemove, self).get_object()

        return o

    def get(self, *args, **kwargs):
        self.product = models.Product.objects.get(pk=self.kwargs.get('p_id', None))

        return super(ImageRemove, self).get(*args, **kwargs)

    def get_queryset(self):
        qs = super(ImageRemove, self).get_queryset()
        try:
            image = qs.get(pk=self.kwargs['pk'])
            self.img_link = image.image
            self.thumb_link = image.thumb
        except self.model.DoesNotExists:
            raise Http404('pk %s does not exists' % self.kwargs['pk'])

        return qs

    def get_context_data(self, **kwargs):
        ctx = super(ImageRemove, self).get_context_data()
        ctx['thumb'] = str(self.thumb_link.url)

        return ctx

# TODO: get all meta data about products, such as getting self.product and etc., and store it in ProductMixin!!!

    def post(self, request, *args, **kwargs):
        self.product = models.Product.objects.get(pk=self.kwargs.get('p_id', None))
        qs = super(ImageRemove, self).post(request, *args, **kwargs)
        os.remove(str(self.img_link.file))
        os.remove(str(self.thumb_link.file))

        return qs

    def get_parent_title(self):
        return 'Продукт: %s' % self.product.name

    def get_parent_link(self):
        return reverse('product_update', kwargs={'pk': self.product.id})

    def get_success_url(self):
        return self.get_parent_link()


class ImageCreateTmp(CreateView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Добавить изображение'
    template_name = 'images/create.html'
    current_page = 'products'

    model = models.ProductImages
    fields = ['image' ]
    context_object_name = 'image'

    def get_context_data(self, **kwargs):
        ctx = super(ImageCreateTmp, self).get_context_data(**kwargs)
        ctx['form'] = self.get_form()

        return ctx

    def get(self, *args, **kwargs):
        self.product = models.Product.objects.get(pk=self.kwargs.get('pk', None))

        return super(ImageCreateTmp, self).get(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        image = self.request.FILES.get('image')
        name = image.name
        splitted = name.split('.')
        ext_idx = len(splitted) - 1
        ext = splitted[ext_idx]
        image.name = str(uuid.uuid4()) + '.' + str(ext)
        self.product = models.Product.objects.get(pk=self.kwargs.get('pk', None))

        return super(ImageCreateTmp, self).post(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super(ImageCreateTmp, self).get_form()

        return form

    def form_invalid(self, form):

        return super(ImageCreateTmp, self).form_invalid(form)

    def form_valid(self, form):
        image = form.save(commit=False)
        if self.product is not None:
            image.product = self.product
            image.save()

            size = (constants.THUMB_SIZE_HEIGHT, constants.THUMB_SIZE_WIDTH)

            full_link = image.image.file.name
            infile = Image.open(full_link)
            outfile = infile.fp.name.split('.')[0] + '_thumb' + '.jpg'

            if infile != outfile:
                try:
                    infile.thumbnail(size)
                    infile.save(outfile, "JPEG")
                except IOError:
                    print("cannot create thumbnail for", infile)

            rel_name_spl = outfile.split('/')
            file_idx = len(rel_name_spl) - 1
            dir_idx = len(rel_name_spl) - 2
            image.thumb = rel_name_spl[dir_idx] + '/' + rel_name_spl[file_idx]
            image.save()
        else:
            return Http404('Product id is None!')


        return super(ImageCreateTmp, self).form_valid(form)

    def get_parent_link(self):
        return reverse('product_update', kwargs={'pk': self.product.id})

    def get_parent_title(self):
        return 'Продукт: %s' % self.product.name

    def get_success_url(self):
        return self.get_parent_link()


class ImageListTmp(ListView, AdminContextMixin, WithHeader, WithParent):
    page_header = 'Изображения'
    current_page = 'products'
    template_name = 'images/list.html'
    context_object_name = 'images'
    ordering = 'id'

    def get(self, *args, **kwargs):
        self.product = models.Product.objects.get(pk=self.kwargs.get('pk', None))

        return super(ImageListTmp, self).get(*args, **kwargs)

    def get_queryset(self, **kwargs):
        images = models.ProductImages.objects.filter(product__id=self.product.id)

        return images

    def get_context_data(self, **kwargs):
        ctx = super(ImageListTmp, self).get_context_data()
        ctx['images'] = self.get_queryset()
        ctx['product_id'] = self.product.id

        return ctx

    def get_parent_link(self):
        return reverse('product_update', kwargs={'pk': self.product.id})

    def get_parent_title(self):
        return 'Продукт: %s' % self.product.name


