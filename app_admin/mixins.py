import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import ContextMixin, View
import datetime


CONST_LOGIN_REDIRECT_URL = '/admin/login/'


@method_decorator(login_required(login_url=CONST_LOGIN_REDIRECT_URL), name='dispatch')
class AdminAuthMixin(View):
    pass


class WithParent(ContextMixin):
    parent_link = None
    parent_title = None
    model = None

    def get_parent_link(self):
        return self.parent_link

    def get_parent_title(self):
        if self.model and not self.parent_title:
            return self.model._meta.verbose_name_plural
        else:
            return self.parent_title


    def get_context_data(self, **kwargs):
        ctx = super(WithParent, self).get_context_data(**kwargs)
        if self.get_parent_link():
            ctx['parent_link'] = self.get_parent_link()
        else:
            raise ValueError("WithParent.parent_link must be defined!")

        if self.get_parent_title():
            ctx['parent_title'] = self.get_parent_title()
        else:
            raise ValueError("WithParent.parent_title must be defined!")

        return ctx


class WithHeader(ContextMixin):
    page_header = None

    def get_page_header(self):
        return self.page_header

    def get_context_data(self, **kwargs):
        ctx = super(WithHeader, self).get_context_data(**kwargs)
        if self.get_page_header():
            ctx['page_header'] = self.get_page_header()
        else:
            raise ValueError("WithHeader.page_header must be defined!")

        return ctx


class AdminContextMixin(ContextMixin, AdminAuthMixin):
    current_page = None
    current_group = None

    def get_context_data(self, **kwargs):
        ctx = super(AdminContextMixin, self).get_context_data(**kwargs)
        if self.current_page:
            ctx['current_page'] = self.current_page
        else:
            raise ValueError("AdminContextMixin.current_page must be defined!")

        ctx['current_group'] = self.current_group
        ctx['year'] = datetime.datetime.now().year
        ctx.update(settings.SITE_SETTINGS)

        return ctx


class CustomerContextMixin(ContextMixin):
    current_page = None
    current_group = None

    def get_context_data(self, **kwargs):
        ctx = super(CustomerContextMixin, self).get_context_data(**kwargs)
        if self.current_page:
            ctx['current_page'] = self.current_page
        else:
            raise ValueError("CustomerContextMixin.current_page must be defined!")

        ctx['current_group'] = self.current_group
        ctx['year'] = datetime.datetime.now().year
        ctx.update(settings.SITE_SETTINGS)

        return ctx


class JSONResponseMixin(object):
    def render_to_response(self, context):
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **kwargs):
        return HttpResponse(content, content_type='application/json', **kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)