# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-17 20:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20160516_0807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderitem',
            name='amount',
            field=models.FloatField(),
        ),
    ]
