# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-14 11:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20160305_2159'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='number',
            field=models.CharField(default='', max_length=100, verbose_name='Номер заказа'),
        ),
        migrations.AddField(
            model_name='order',
            name='total',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='order',
            name='user_email',
            field=models.CharField(default='', max_length=100, verbose_name='Email'),
        ),
        migrations.AddField(
            model_name='order',
            name='user_phone',
            field=models.CharField(blank=True, default='', max_length=100, null=True, verbose_name='Телефон'),
        ),
        migrations.AddField(
            model_name='productimages',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='productimages',
            name='thumb',
            field=models.ImageField(blank=True, default='', null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='product',
            name='max_saleable_amount',
            field=models.FloatField(default='1000', verbose_name='Максимальное кол-во единиц для реализации'),
        ),
    ]
