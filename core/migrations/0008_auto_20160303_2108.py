# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-03 21:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_unit_short_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unit',
            name='short_name',
            field=models.CharField(max_length=20, unique=True, verbose_name='Сокращенные ед. изм.'),
        ),
    ]
