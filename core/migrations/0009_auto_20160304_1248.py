# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-04 12:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20160303_2108'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('status', models.CharField(max_length=50, verbose_name='Статус заказа')),
            ],
            options={
                'verbose_name_plural': 'Статусы заказа',
                'verbose_name': 'Статус заказа',
                'db_table': 'order_statuses',
            },
        ),
        migrations.AlterModelOptions(
            name='order',
            options={'ordering': ['-date'], 'verbose_name': 'Заказ', 'verbose_name_plural': 'Заказы'},
        ),
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.ForeignKey(db_column='status_id', default=1, on_delete=django.db.models.deletion.CASCADE, to='core.OrderStatus', verbose_name='Статус заказа'),
        ),
    ]
