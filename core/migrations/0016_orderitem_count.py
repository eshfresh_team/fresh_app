# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-17 21:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20160517_2044'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='count',
            field=models.PositiveIntegerField(default=1),
        ),
    ]
