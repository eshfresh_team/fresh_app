# -*- coding: utf-8 -*-

from django.db import models
from datetime import datetime, timedelta
from django.db.utils import IntegrityError
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from .const import *

class Unit(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30, blank=False, null=False, unique=True, verbose_name='Единица')
    short_name = models.CharField(max_length=20, blank=False, null=False, unique=True, verbose_name='Короткое название')

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'short_name': self.short_name
        }

    def __str__(self):
        return self.short_name

    class Meta:
        db_table = 'units'
        verbose_name = 'Единица измерения'
        verbose_name_plural = 'Единицы измерении'


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False, null=False, unique=True, verbose_name='Город')
    timezone = models.CharField(max_length=30, blank=True, null=True, verbose_name='Часовой пояс')

    def __str__(self):
        if self.timezone:
            return "%s %s" % (self.name, self.timezone)
        else:
            return self.name

    class Meta:
        db_table = 'cities'
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=False, null=False, blank=False, max_length=255, verbose_name='Название')
    description = models.TextField(max_length=1500, unique=False, null=True, blank=True, verbose_name='Описание')
    category = models.ForeignKey('Category', to_field='id', db_column='category_id', verbose_name='Категория')
    units = models.ForeignKey('Unit', to_field='id', db_column='unit_id', verbose_name='Ед. изм')
    seller = models.ForeignKey('User', to_field='id', db_column='seller_id', null=False, blank=False,
                               verbose_name='Продавец')
    minimal_amount = models.FloatField(unique=False, blank=False, null=False, default='1',
                                       verbose_name='Единица реализации')
    price_per_min_amount = models.FloatField(unique=False, blank=False, null=False,
                                             verbose_name='Цена за единицу')
    max_saleable_amount = models.FloatField(unique=False, blank=False, null=False, default='1000',
                                            verbose_name='Максимальное кол-во единиц для реализации')
    left_in_stock = models.IntegerField(default=1, unique=False, blank=False, null=False,
                                        verbose_name='Доступный остаток')
    enabled = models.PositiveIntegerField( default=PRODUCT_ENABLED, unique=False, null=False,
                                           blank=False, verbose_name='Продукт включен')
    # это поле сообщается поставщиком, измеряется в днях
    delivery_time = models.PositiveIntegerField(unique=False, blank=False, null=False, default=3,
                                                verbose_name='Число дней для возобновления поставок')

    def __str__(self):
        return self.name

    @property
    def arrival_at(self):
        if self.is_in_stock:
            delta = timedelta(days=1)
        else:
            delta = timedelta(days=int(self.delivery_time))
        date = datetime.now()
        return date + delta

    @property
    def is_in_stock(self):
        return self.left_in_stock > 0

    def save(self, *args, **kwargs):
        if self.seller and self.seller.type_of is not USER_TYPE_SELLER:
            raise IntegrityError("Product seller must be with type USER_TYPE_SELLER!")

        return super(Product, self).save(*args, **kwargs)

    @property
    def images(self):
        images = ProductImages.objects.filter(product__id=self.id)
        i = []
        for x in images:
            i.append({'id': x.id, 'thumb': x.thumb.url, 'large': x.image.url})
        return i

    def to_json(self):
        if not self.id:
            return {}

        return {
            'id':self.id,
            'name': self.name,
            'description': self.description,
            'price_per_min_amount': self.price_per_min_amount,
            'units': self.units.to_json()
        }

    class Meta:
        db_table = 'products'
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'


class ProductImages(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=timezone.now)
    product = models.ForeignKey('Product', to_field='id', db_column='product_id',
                                unique=False, blank=False, null=False)
    thumb = models.ImageField(blank=True, null=True, default='')
    image = models.ImageField(upload_to='uploads/')

    class Meta:
        db_table = 'product_image'
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


class OrderItem(models.Model):
    id = models.AutoField(primary_key=True)
    '''
    если товар стоит 100 р за 0.5 кг, и в корзине лежит 1.5 кг, то count=3 (3 минимальных объема реализации), а amount=1.5 !
    '''
    count = models.PositiveIntegerField(blank=False, null=False, unique=False, default=1) # это количество минимальных объемов товара
    amount = models.FloatField(blank=False, null=False, unique=False) # а это количество самого товара
    product = models.ForeignKey('Product', to_field='id', db_column='product_id', blank=False, null=False, unique=False)
    order = models.ForeignKey('Order', to_field='id', db_column='order_id', blank=False, null=False, unique=False)
    _price = models.FloatField(blank=False, null=False, unique=False, default=1)

    @property
    def price(self):
        return self.product.price_per_min_amount * self.count

    def save(self, *args, **kwargs):
        if not self.amount:
            raise ValueError('Нельзя добавить позицию заказа без указания количества')
        if self.price != self._price:
            raise ValueError('Цена позиции неверна!')

        super(OrderItem, self).save(*args, **kwargs)

    def to_json(self):
        return {
            'product': self.product.to_json(),
            'amount': self.amount,
            'count': self.count,
            'price': self.price
        }

    class Meta:
        db_table = 'order_items'
        verbose_name = 'Позиция заказа'
        verbose_name_plural = 'Позиции заказа'
        unique_together = (('product', 'order'),)


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey('User', to_field='id', db_column='user_id', blank=True, null=True, unique=False,
                                 verbose_name='Клиент')
    total = models.PositiveIntegerField(null=False, blank=False, unique=False, default=0)
    number = models.CharField(null=False, blank=False, max_length=100, default='', unique=False,
                              verbose_name='Номер заказа')
    user_phone = models.CharField(null=True, blank=True, max_length=100, default='', unique=False,
                                  verbose_name='Телефон')
    user_email = models.CharField(null=True, blank=True, max_length=100, default='', unique=False,
                                  verbose_name='Email')
    date = models.DateTimeField(default=timezone.now, verbose_name='Дата заказа')
    discount = models.FloatField(blank=False, null=False, unique=False, default=.0, verbose_name='Скидка')
    comment = models.TextField(max_length=512, blank=True, null=True, unique=False, verbose_name='Комментарий к заказу')
    status = models.PositiveSmallIntegerField(default=ORDER_STATUS_INITIAL, db_column='status', blank=False, null=False,
                                              unique=False, verbose_name='Статус заказа')
    # TODO: recalculate Product.left_in_stock at Order creating

    def to_json(self):
        if not self.id:
            return {}

        order_items = OrderItem.objects.filter(order_id=self.id)
        j = { 'items': [] }

        for oi in order_items:
            j['items'].append(oi.to_json())

        j['phone'] = self.user_phone
        j['number'] = self.number
        j['total'] = self.total
        if self.customer:
            j['email'] = self.customer.email
        else:
            j['email'] = self.user_email
        j['date'] = self.date.strftime('%d.%m.%Y %H:%M')

        return j

    @property
    def get_person(self):
        if self.customer:
            return self.customer.email
        else:
            return self.user_email

    @property
    def user_registered(self):
        return self.customer is not None

    class Meta:
        ordering = ['-date']
        db_table = 'orders'
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class InnerUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('User must have an email')

        user = self.model(
                email=self.normalize_email(email)
        )

        user.type_of = USER_TYPE_CUSTOMER
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.type_of = USER_TYPE_ADMIN
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.CharField(max_length=255, unique=True)
    first_name = models.CharField(max_length=50, null=True, default="", unique=False, verbose_name='Имя')
    last_name = models.CharField(max_length=50, null=True, default="", unique=False, verbose_name='Фамилия')
    phone = models.CharField(max_length=15, null=True, blank=True, unique=True, verbose_name='Телефон')
    type_of = models.IntegerField(default=USER_TYPE_CUSTOMER, null=False, blank=False)
    city = models.ForeignKey('City', to_field='id', db_column='city_id', null=True, blank=True, unique=False,
                             verbose_name='Город')
    address = models.CharField(max_length=255, null=True, blank=True, unique=False, verbose_name='Адрес')
    lat = models.FloatField(default=.0, null=True, blank=True, unique=False)
    lon = models.FloatField(default=.0, null=True, blank=True, unique=False)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = InnerUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.get_full_name

    @property
    def get_full_name(self):
        if not self.first_name or not self.last_name:
            return self.email
        else:
            return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        if not self.first_name:
            return self.email
        else:
            return '%s (%s)' % (self.first_name, self.email)

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def full_address(self):
        if self.city and self.address:
            return "%s, %s" % (self.city.name, self.address)
        if self.city:
            return self.city.name
        return ""

    def get_categories(self):
        if self.type_of is not USER_TYPE_SELLER:
            return []
        else:
            return SellerCategory.objects.filter(user_id=self.id)

    @property
    def products(self):
        if self.type_of is not USER_TYPE_SELLER:
            return []
        else:
            return Product.objects.filter(seller_id=self.id)

    @property
    def prod_count(self):
        if self.type_of is not USER_TYPE_SELLER:
            return 0
        else:
            return len(self.products)

    class Meta:
        db_table = 'users'
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False, null=False, unique=True, verbose_name='Категория')
    sellers = models.ManyToManyField(User, through='SellerCategory')

    def get_sellers_in_category(self):
        sellers_in_cat = SellerCategory.objects.filter(category__id=self.id)
        sellers = []
        for s in sellers_in_cat:
            sellers.append(s.seller)
        return sellers
        # return [s.seller for s in SellerCategory.objects.filter(category_id=self.id)]

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


# TODO: rename to SellerCategory
class SellerCategory(models.Model):
    id = models.AutoField(primary_key=True)
    seller = models.ForeignKey('User', to_field='id', db_column='seller_id', null=False, blank=False, unique=False)
    category = models.ForeignKey('Category', to_field='id', db_column='category_id',
                                 null=False, blank=False, unique=False)

    def save(self, *args, **kwargs):
        if self.seller.type_of != USER_TYPE_SELLER:
            raise ValueError("Cannot create SellerCategory, seller_pk points to non-seller User.")

        return super(SellerCategory, self).save(*args, **kwargs)

    class Meta:
        db_table = 'seller_categories'
