import random
import string

from django.db.utils import IntegrityError
from django.test import TestCase
from . import models as m

# Create your tests here.
def get_rand_string(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def make_email():
    user = get_rand_string(10)
    domain = get_rand_string(6)
    zone = get_rand_string(2)

    return "%s@%s.%s" % (user, domain, zone)

def make_seller(type_of=m.USER_TYPE_SELLER):
    s = m.User()
    s.type_of = type_of
    s.email = make_email()
    s.save()

    return s


def get_product():
    u = m.Unit(name='кг')
    u.save()
    cat = m.Category(name='Продукты')
    cat.save()
    p = m.Product(name='Молоко')
    p.units = u
    p.category = cat
    p.minimal_amount = 1.0
    p.price_per_min_amount = 100.0

    p.seller = make_seller()
    p.save()

    return p


def get_order():
    om = m.Order()
    om.customer = make_seller(m.USER_TYPE_CUSTOMER)
    om.comment = '!'
    om.summ = 100.0
    om.discount = 5.0

    return om


class UnitsTest(TestCase):
    def test_create(self):
        u = m.Unit()
        u.name = 'кг'
        u.save()

        assert str(u) == 'кг'

    def test_create_no_name(self):
        u = m.Unit()
        u.name = None
        try:
            u.save()
        except IntegrityError:
            return
        except Exception as e:
            self.fail('Unhandled exception %s' % str(e))
        else:
            self.fail('Must raise IntegrityError on empty name')


class ProductTest(TestCase):
    def test_create_empty(self):
        p = m.Product()

        try:
            p.save()
        except Exception as e:
            print("Fail with %s, OK" % e)
        else:
            self.fail("Must fire exception")

    def test_create(self):
        p = get_product()
        assert str(p) == 'Молоко'

    def test_create_product_no_seller(self):
        i_am_no_seller = m.User()
        i_am_no_seller.type_of = m.USER_TYPE_CUSTOMER
        i_am_no_seller.email = make_email()
        # i_am_no_seller.email = 'qwer@ty.io'
        i_am_no_seller.save()

        u = m.Unit(name='кг')
        u.save()
        cat = m.Category(name='Продукты')
        cat.save()
        p = m.Product(name='Молоко')
        p.units = u
        p.category = cat
        p.minimal_amount = 1.0
        p.price_per_min_amount = 100.0

        try:
            p.seller = i_am_no_seller
            p.save()
        except IntegrityError:
            return
        except Exception as e:
            self.fail("Wait for IntegrityError, got %s" % e)
        else:
            self.fail("Wait for IntegrityError, got nothing")

    def test_product_images(self):
        p = get_product()

        img1 = m.ProductImages()
        img1.product = p
        img1.image.name = '/tmp/1.jpg'
        img1.save()

        img2 = m.ProductImages()
        img2.product = p
        img2.image.name = '/tmp/2.jpg'
        img2.save()

        assert len(p.get_images()) == 2


class CityTest(TestCase):
    def test_empty_name_create(self):
        try:
            c = m.City(name=None)
            c.save()
        except IntegrityError:
            return
        except Exception as e:
            self.fail('Wait for IntegrityError, got %s' % e)
        else:
            self.fail('Wait for IntegrityError, got nothing')

    def test_create(self):
        try:
            c = m.City()
            c.name = "Мухосранск"
            c.save()
        except Exception as e:
            self.fail('Something gone wrong: %s' % e)
        else:
            return


class OrderItemTest(TestCase):
    def test_empty_create(self):
        try:
            oi = m.OrderItem()
            oi.amount = 1.0
            oi.save()
        except IntegrityError:
            return
        except Exception as e:
            self.fail('Should be an IntegrityError, got %s' % e)

        else:
            self.fail('Should be an IntegrityError, got nothing')

    def test_invalid_amount_create(self):
        try:
            oi = m.OrderItem()
            oi.product = get_product()
            oi.order = get_order()
            oi.amount = -1.1
            oi.save()
        except ValueError:
            return
        except Exception as e:
            self.fail('Should be an ValueError, got %s' % e)

        else:
            self.fail('Should be an ValueError, got nothing')
