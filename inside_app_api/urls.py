from django.conf.urls import url, include
from rest_framework import routers
from inside_app_api import views

router = routers.DefaultRouter()
# router.register(r'products_json', views.ProductViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^products_json/$',  views.ProductViewSet.as_view({'get': 'list'})),
]