from django.apps import AppConfig


class InsideAppApiConfig(AppConfig):
    name = 'inside_app_api'
