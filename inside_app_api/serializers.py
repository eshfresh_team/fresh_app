from core import models
from rest_framework import serializers


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    units = serializers.StringRelatedField(many=False)
    seller = serializers.StringRelatedField(many=False)

    class Meta:
        model = models.Product
        fields = ('id', 'name', 'description', 'minimal_amount', 'price_per_min_amount', 'max_saleable_amount',
                  'is_in_stock', 'left_in_stock', 'images', 'units' ,'seller', 'arrival_at')

