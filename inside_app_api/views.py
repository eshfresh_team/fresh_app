from core import models
from rest_framework import viewsets
from inside_app_api.serializers import ProductSerializer
import random


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows products to be viewed or edited.
    """
    queryset = models.Product.objects.filter(enabled=1).order_by('id')
    serializer_class = ProductSerializer


class SuggestedProductsViewSet(viewsets.ModelViewSet):
    count = 2
    r = random.randrange(0, 1)
    queryset = models.Product.objects.all()[:]

