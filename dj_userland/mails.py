from django.template import Template, Context


def mail_body(ctx):
    t = Template('''<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <title>Заказ</title>
                    </head>
                    <body>
                      <p>Добрый день! Только что вы оставили заказ на сайте ешьфреш.рф</p>
                      <p>Дата заказа: {{ order.date }}</p>
                      <p>Номер заказа: {{ order.number }}</p>
                      Позиции:
                      <table class="table">
                        <thead>
                          <tr>
                            <th style="padding: 8px; border-bottom: 2px solid #ddd;">Наименование</th>
                            <th style="padding: 8px; border-bottom: 2px solid #ddd;">Количество</th>
                            <th style="padding: 8px; border-bottom: 2px solid #ddd;">Цена</th>
                            <th style="padding: 8px; border-bottom: 2px solid #ddd;">Стоимость</th>
                          </tr>
                        </thead>
                        <tbody>
                          {% for oi in order.items %}
                          <tr>
                            <td style="padding: 8px; border-top: 1px solid #ddd;">{{ oi.product.name }}</td>
                            <td style="padding: 8px; border-top: 1px solid #ddd;">{{ oi.amount }}{{ oi.product.units.short_name }}</td>
                            <td style="padding: 8px; border-top: 1px solid #ddd;">{{ oi.product.price_per_min_amount }}</td>
                            <td style="padding: 8px; border-top: 1px solid #ddd;">{{ oi.price }}</td>
                          </tr>
                          {% endfor %}
                        </tbody>
                      </table>
                      <p style="color: #0080EB;">Сумма заказа: {{ order.total }} Р<i class="fa fa-ruble"></i></p>
                      <p>Спасибо за заказ, мы вам перезвоним!</p>
                    </body>
                    </html>''')
    c = Context(ctx)
    mail = t.render(c)

    return mail