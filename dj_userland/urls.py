from django.conf.urls import url, include
from django.conf.urls.static import static
from app_admin import urls as admin_urls
from app_customer import urls as customer_urls
from inside_app_api import urls as api_urls
from django.conf import settings


urlpatterns = [
    url(r'^admin/', include(admin_urls.patterns)),
    url(r'^', include(customer_urls.patterns)),
    url(r'^api/', include(api_urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)